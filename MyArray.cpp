

/*---------------------------------------------------------------------
 *
 * MyArray.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * MyArray class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "MyArray.h"
 
/*------------------------- Implementation ---------------------------*/

/* MyArray constructor. */
MyArray::MyArray(int initial_size)
{
    size = initial_size;
    data = new int[size];
}

/* Copy constructor */
MyArray::MyArray(const MyArray& copy_from)
{
    size = copy_from.size;
    data = (int*) new int[size];

    for(unsigned int i = 0; i < copy_from.size; i += 1)
    {
        data[i] = copy_from.data[i];
    }
}

/* Copy assignment */
MyArray& MyArray::operator=(const MyArray& copy_from)
{
    data = copy_from.data;
    size = copy_from.size;
}

/* Getters */
unsigned int MyArray::get_size(void)
{
    return this->size;
}

unsigned int MyArray::get_data(unsigned int index, int* return_data)
{
    // Make sure not to access outside the array.
    if(index >= this->size)
    {
        return 1;
    }
    else
    {
        *return_data = this->data[index];
        return 0;
    }
}

/* Setters */
unsigned int MyArray::set_data(unsigned int index, int set_data)
{
    // Make sure not to access outside the array.
    if(index >= this->size)
    {
        return 1;
    }
    else
    {
        this->data[index] = set_data;
        return 0;
    }
}

