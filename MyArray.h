
/*---------------------------------------------------------------------
 *
 * Date.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * MyArray class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef MYARRAY_H
#define MYARRAY_H

/*------------------------- Implementation ---------------------------*/
 
class MyArray
{
    private:
        int* data;
        unsigned int size;

    public: 
        MyArray(int size);
        MyArray(const MyArray& copy_from);
        MyArray& operator=(const MyArray& copy_from);

        unsigned int get_size(void);
        unsigned int get_data(unsigned int index, int* return_data);

        unsigned int set_data(unsigned int index, int set_data);

};

#endif

