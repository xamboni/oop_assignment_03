
/*---------------------------------------------------------------------
 *
 * assignment_03.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Introduction to Copy Constructors
 * 
 * NOTE: Compile: `g++ -std=c++11 -o assignment_03 assignment_03.cpp MyArray.cpp`
 *
 ---------------------------------------------------------------------*/
 
/*---------------------------- Includes ------------------------------*/
 
/* Standard */
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>

/* User */
#include "MyArray.h"
 
/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/
 
/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

int print_array(MyArray this_array, unsigned int size);

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: None 
 * Outputs: MyArray contents and copy constructed MyArray contents.
 * Note: 
 * 
----------------------------------------------------------------------*/
int main (int argc, char* argv[])
{

    int result = 0;
    unsigned int size = 0;

    // Create a new array object on the heap with space for 10 chars.
    size = 10;
    MyArray* existingObject = new MyArray(size);
    
    // Initialize array contents. 
    size = existingObject->get_size();
    for(unsigned int i = 0; i < size; i += 1)
    {
        int result = 0;

        result = existingObject->set_data( i, (int) i);
        if(result)
        {
            std::cout << "Error setting array data." << std::endl;
            break;
        }
    }

    // Print array contents. 
    std::cout << "Printing original array data." << std::endl;
    result = print_array(*existingObject, size);
    if (result)
    {
        std::cout << "Error printing the array." << std::endl;
    }

    // Attempt copy constructor.
    MyArray newobj = *existingObject;

    // Print copy constructed array contents. 
    size = newobj.get_size();

    std::cout << "Printing copy constructed array data." << std::endl;
    result = print_array(newobj, size);
    if (result)
    {
        std::cout << "Error printing the array." << std::endl;
    }


    return result;
}

/*-------------------------------------------------------------------*/

/*
 * Inputs: The array holding data and the size of printing desired. 
 * Outputs: Data printed of size desired starting from the beginning.
 * Note: 
 * 
----------------------------------------------------------------------*/
int print_array(MyArray this_array, unsigned int size)
{
    // Print array contents. 
    for(unsigned int i = 0; i < size; i += 1)
    {
        int data = 0;
        int result = 0;

        result = this_array.get_data(i, &data);
        if (!result)
        {
            std::cout << data;
        }
        else
        {
            // Something went wrong.
            return 1;
        }
    }
    std::cout << std::endl;
    return 0;
}

